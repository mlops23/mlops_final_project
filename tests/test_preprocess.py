from src.data.preprocess import preprocess_for_train
from click.testing import CliRunner

runner = CliRunner()


def test_cli_command():
    result = runner.invoke(preprocess_for_train, ['data/raw/train.csv', 'data/interim/train_interim.csv'])
    assert result.exit_code == 0
