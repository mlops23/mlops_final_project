# Полное описание проекта курса MLOps и production подход к ML исследованиям

Проект выполнен в Pycharm, Windows 10.

Последовательность действий:
1. Устанавливается Python (3.9.7).
2. Устанавливается и [настраивается](https://www.jetbrains.com/help/pycharm/poetry.html) poetry для управления зависимостями.
3. Ставим необходимые библиотеки - зависимости в pyproject.toml
4. [cookiecutter](https://mlops-guide.github.io/Structure/starting/) не использую - для полного погружения в структуру проекта.
5. Инициализируем Git, заливаем пустой проект с ноутбуками в репозиторий в GitLab.
6. Подготавливаем свой раннер в GitLab:
   1. В папке проекта переходим Settings -> CI/CD.
   2. Отключаем встроенные раннеры и [создаем свой](https://docs.gitlab.com/runner/install/).
   3. [Регистрируем](https://docs.gitlab.com/runner/register/) и настраеваем его.
   4. Выбираем тип ранера - docker, образ по умолчанию - python:3.9.13-slim.
7. Готовим скрипты из ноутбуков. Гиперпараметры модели и общие параметры экспериментов вынесены в отдельный файл lgbm_params.yml.
8. Готовим код-форматтер - используем yapf. Так как я привык к форматированию google - использую его. Конфигурация записывается в pyproject.toml. Нужно установить toml, чтобы yapf спарсил конфиг. 
Пример изменений в файле: ```yapf -i src/data/preprocess.py```
9. Прикручиваем CLI и проверяем, что скрипты отдельно друг от друга отрабатывают:
   1. Обработка сырых данных (общаяя для трейна и теста) ```python -m src.data.preprocess data/raw data/interim```
   2. Подготовка обучающих данных ```python -m src.features.prepare_to_train data/interim data/processed```
   3. Обучени модели ```python -m src.model.train data/processed lgbm_params.yml models reports```
10. Поднимаем minio и nginx.
11. Настраиваем работу с dvc:
    1. Устанавливаем - ```poetry add bcrypt 'dvc[s3]'```.
    2. Подключаем предварительно подготовленный в minio bucket - ```dvc remote add minio s3://mlops-project```
12. Добавляем postgresql и pgadmin, фиксируем их версии в docker-compose файле.
13. В pgadmin создать сервер, ip взять в контейнере с postgres ```docker inspect <postgres_container>, взять IP-address```
14. Создаем Dockerfile и билдим образ mlflow ```docker build -f docker_images/mlflow_image/Dockerfile -t mlflow_server```
15. Для работы с boto необходимо зайти в папку users, под своего пользователя и создать папку .aws (пример мой - C:\Users\svrus\.aws).
В этой папке нужно создать файл credentials, в который прописать aws_access_key_id, aws_secret_access_key, aws_bucket_name.
([из 7 урока](https://youtu.be/KZ5Cdevd-b8) 48 минута
16. Меняем гиперпараметры, создавая этим несколько наглядных результатов эксперимента в MLFlow.
17. Проверяем, что все файлы создаются и база данных пишется.
18. Модель как сервис из mlflow:
    1. можно запустить сервис, например, ```mlflow models serve --env-manager=local -m s3://mlops-project/7/96dc470d881a4782a767ddf901348cea/artifacts/lgbm_model -h 0.0.0.0 -p 8001```. Для его работы необходимо записать в системные переменные MLFLOW_S3_ENDPOINT_URL (через bash export MLFLOW_S3_ENDPOINT_URL=http://localhost:9000)
    2. Ввести в командной строке ```mlflow models serve --help```, или зайти в официальную документацию, чтобы узнать, как
    с помощью curl получать предикты.
    3. Можно сбилдить докер образ и уже крутить его как сервис:
    ```mlflow models build-docker -m s3://mlops-project/7/96dc470d881a4782a767ddf901348cea/artifacts/lgbm_model -n "lgbm-model"```
19. Готовим приложение на FastAPI.
20. Собираем образ model_as_service:
    ```docker build -f docker_images/model_as_service/Dockerfile -t model_as_service:final_project .```
21. Делаем post запрос ```curl -i -X POST -H "Content-Type: multipart/form-data" -F "file=@data/raw/test.csv" http://localhost:8003/invocations```
22. Добавляем тесты, используем pytest.
23. Добавляем в docker-compose nexus, настраиваем репозиторий в нем (пароль в подключенной папке!!), ставим галочки
If checked, the repository accepts incoming requests и Allow clients to use the V1 API to interact with this repository,
http порт 8213.
24. Регистрируем его в docker: ```docker login -u admin -p <password> 127.0.0.1:8123```
25. Добавляем инфо о приложении в локальный хаб: ```docker tag model_as_service:final_project 127.0.0.1:8123/mlflow_as_service:latest```
26. Пушим образ ```docker push 127.0.0.1:8123/mlflow_as_service:latest```




    