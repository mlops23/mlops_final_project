# Предсказание стоимости жилья (и немного MLops)

Этот проект был выполнен более двух лет назад, благополучно заброшен до лучших времен. И вот время настало))

Ссылка на [соревнование kaggle](https://www.kaggle.com/c/house-prices-advanced-regression-techniques).

[Полное описание проекта](https://gitlab.com/mlops23/mlops_final_project/-/blob/master/project_docs/main_doc.md)
