import os
import json
import yaml
import click
import pickle
import joblib
import mlflow
import pandas as pd
from dotenv import load_dotenv
import lightgbm as lgb
from mlflow.models.signature import infer_signature
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score

# подгружаем переменные из .env
load_dotenv()


def calc_rmse(target: pd.Series, predict: pd.Series) -> float:
    """
    Вычисление RMSE для валидации предсказаний модели

    Args:
        target: истинные значения.
        predict: предсказанные значения.

    Returns:
        значение метрики
    """

    return (mean_squared_error(target, predict))**0.5


@click.command()
@click.argument('path_to_processed', type=click.Path(exists=True))
@click.argument('path_to_config', type=click.Path())
@click.argument('path_to_models', type=click.Path())
@click.argument('path_to_metrics', type=click.Path())
def train(path_to_processed: str, path_to_config: str, path_to_models: str,
          path_to_metrics: str) -> None:
    """
    Обучение модели LGBMRegressor на подготовленных данных

    Args:
        path_to_processed: путь к файлам pickle с признаками и таргетами.
        path_to_config: путь к конфигу с параметрами модели.
        path_to_models: путь сохранения модели (с именем файла).
        path_to_metrics: путь к сохраняемым метрикам.

    Returns:
    """

    # задаем имя эксперимента и uri сервера для mlflow
    mlflow.set_experiment('final_project_models')
    mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))

    # подгружаем данные для обучения
    with open(f'{path_to_processed}/features.pkl', 'rb') as file:
        features = pickle.load(file)

    with open(f'{path_to_processed}/target.pkl', 'rb') as file:
        target = pickle.load(file)

    with open(path_to_config, 'r') as file:
        params = yaml.safe_load(file)

    X_train, X_val, y_train, y_val = train_test_split(
        features,
        target,
        test_size=params['test_size'],
        random_state=params['random_state'])

    lgb_train = lgb.Dataset(X_train, y_train)
    lgb_eval = lgb.Dataset(X_val, y_val, reference=lgb_train)

    # определяем объект модели
    model = lgb.train(params['model_params'],
                      lgb_train,
                      num_boost_round=200,
                      valid_sets=lgb_eval,
                      verbose_eval=False,
                      early_stopping_rounds=30)

    predict = model.predict(X_val, num_iteration=model.best_iteration)

    # запоминаем входные и выходные данные модели
    signature = infer_signature(X_val.to_numpy(), predict)

    # пишем скоры
    scores = {
        'rmse': calc_rmse(y_val, predict),
        'r2_score': r2_score(y_val, predict)
    }

    # сохраняем метрики и модель
    with open(f"{path_to_metrics}/{params['metrics_save_name']}.json",
              'w') as file:
        json.dump(scores, file)

    joblib.dump(model, f"{path_to_models}/{params['model_save_name']}.pkl")

    # трекаем параметры и модель
    mlflow.log_params(params['model_params'])
    mlflow.log_metrics(scores)
    mlflow.lightgbm.log_model(lgb_model=model,
                              artifact_path='lgbm_model',
                              registered_model_name='new_model',
                              signature=signature)


if __name__ == '__main__':
    train()
