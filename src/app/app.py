import os
import pickle
import mlflow
import pandas as pd
from dotenv import load_dotenv
from .preprocess import preprocess
from fastapi import FastAPI, File, UploadFile, HTTPException

# подгружаем переменные, устанавливаем переменную MLFLOW_S3_ENDPOINT_URL
# для доступа к бакету с моделями
load_dotenv()
os.environ['MLFLOW_S3_ENDPOINT_URL'] = os.getenv("MLFLOW_S3_ENDPOINT_URL")

# инициализируем приложение
app = FastAPI()


# Определяем класс модели
class Model:

    def __init__(self, model_name: str, model_stage: str):
        """
        Инициализация модели
        Args:
            model_name: имя модели в реестре MLflow
            model_stage: статус модели

        """
        # загружаем модель из реестра моделей MLflow
        self.model = mlflow.lightgbm.load_model(
            f"models:/{model_name}/{model_stage}")

    def predict(self, data: pd.DataFrame):
        """
        Обработка принимаемых данных и предсказание модели

        Args:
            data: датафрейм pandas, необходима предобработка

        Returns:

        """
        # подгружаем типы данных
        with open('/code/app/dtypes_features.pkl', 'rb') as file:
            dtypes_dict = pickle.load(file)

        # обрабатываем данные
        data = preprocess(data)

        for col in dtypes_dict:
            data[col] = data[col].astype(dtypes_dict[col])

        # генерим предикты
        predictions = self.model.predict(data)
        return predictions


# инициализируем модель - передаем имя регистра и состояние
model = Model("new_model", "Production")


# создаем точку входа '/invocations'
@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    # чекаем, что файл csv
    if file.filename.endswith(".csv"):
        # создаем временный файл и делаем из него
        # CSV файл, подгружаемый как pandas Dataframe
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        return list(model.predict(data))
    else:
        # вызываем ошибку HTTP 400 Exception, Bad Request
        raise HTTPException(
            status_code=400,
            detail="Не правильный формат файла. Только для csv.")


# проверка наличия данных доступа к сервисам aws
if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv(
        "AWS_SECRET_ACCESS_KEY") is None:
    exit(1)
