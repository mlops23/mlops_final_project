import click
import pandas as pd


def preprocess(df: pd.DataFrame) -> pd.DataFrame:
    """
    Общая очистка данных.
    Функция принимает подгруженный датафрейм, обрабатывает и возвращает его.

    Args:
        df: датасет для обработки

    Returns:

    """
    if 'Id' in df.columns:
        df = df.drop('Id', axis=1)

    # обработка аномалий и заполнение пропусков
    df['MSZoning'] = df['MSZoning'].fillna('RL')
    df = df[df['LotFrontage'] < 200]
    df['LotFrontage'] = df['LotFrontage'].fillna(df['LotFrontage'].median())
    df = df[df['LotArea'] < 60000]

    df['Alley'] = df['Alley'].fillna('Pave', inplace=True)

    df['Utilities'] = df['Utilities'].fillna('AllPub')
    df.loc[df['HouseStyle'] == '2.5Fin', 'HouseStyle'] = '1.5Fin'

    df = df[df['YearBuilt'] > 1878]
    df.loc[df['RoofStyle'] == 'Shed', 'RoofStyle'] = 'Mansard'
    df = df[~df['RoofMatl'].isin(['Membran', 'Roll'])]
    df = df[~df['Exterior1st'].isin(['ImStucc', 'Stone'])]
    df['Exterior1st'] = df['Exterior1st'].fillna('Plywood')

    df = df[df['Exterior2nd'] != 'Other']
    df['Exterior2nd'] = df['Exterior2nd'].fillna('Plywood')
    df = df[df['MasVnrArea'] < 1290]

    fill_mean = df[(df['Exterior2nd'] == 'VinylSd') &
                   (df['Exterior1st'] == 'VinylSd')]['MasVnrArea'].mean()
    df['MasVnrArea'] = df['MasVnrArea'].fillna(fill_mean)
    df['MasVnrType'] = df['MasVnrType'].fillna('None')

    # обработка оставшихся пропущенных значений
    def del_nan(colname, *args):
        colname1, val1, colname2, val2, val_new = args
        df.loc[(df[colname1] == val1) & (df[colname2] == val2) &
               df[colname].isnull(), colname] = val_new

    del_nan('BsmtQual', 'ExterCond', 'TA', 'ExterQual', 'TA', 'TA')
    del_nan('BsmtQual', 'ExterCond', 'Fa', 'ExterQual', 'TA', 'TA')
    del_nan('BsmtQual', 'ExterCond', 'Fa', 'ExterQual', 'Fa', 'TA')
    del_nan('BsmtQual', 'ExterCond', 'TA', 'ExterQual', 'Gd', 'Gd')
    del_nan('BsmtQual', 'ExterCond', 'Gd', 'ExterQual', 'TA', 'TA')
    del_nan('BsmtQual', 'ExterCond', 'Po', 'ExterQual', 'Fa', 'Fa')
    del_nan('BsmtQual', 'ExterCond', 'TA', 'ExterQual', 'Fa', 'Fa')

    df.loc[df['BsmtCond'].isnull(), 'BsmtCond'] = 'TA'
    df.loc[df['BsmtExposure'].isnull(), 'BsmtExposure'] = 'TA'
    df.loc[df['BsmtFinType1'].isnull(), 'BsmtFinType1'] = 'Unf'
    df.loc[df['BsmtFinType2'].isnull(), 'BsmtFinType2'] = 'Unf'

    for i in ['BsmtFullBath', 'BsmtHalfBath', 'BsmtFinSF1', 'BsmtFinSF2']:
        df[i] = df[i].fillna(0)

    df['BsmtUnfSF'] = df['BsmtUnfSF'].fillna(df['BsmtUnfSF'].mean())
    df['TotalBsmtSF'] = df['TotalBsmtSF'].fillna(df['TotalBsmtSF'].mean())

    df = df[df['Heating'] != 'OthW']

    df['CentralAir'] = df['CentralAir'].map({'Y': 1, 'N': 0})

    df = df[df['Electrical'] != 'Mix']
    df['Electrical'] = df['Electrical'].fillna('SBrkr')
    df.loc[df['FullBath'] == 4, 'FullBath'] = 3
    df['KitchenQual'] = df['KitchenQual'].fillna('TA')

    df['Functional'] = df['Functional'].fillna('Typ')
    df.loc[df['Functional'] == 'Sev', 'Functional'] = 'Typ'

    df['FireplaceQu'] = df['FireplaceQu'].fillna('Other')
    df['GarageType'] = df['GarageType'].fillna('Other')
    df['GarageYrBlt'] = df['GarageYrBlt'].fillna(2000)
    df['GarageFinish'] = df['GarageFinish'].fillna('Othr')
    df['GarageQual'] = df['GarageQual'].fillna('Other')
    df['GarageCond'] = df['GarageCond'].fillna('Other')
    df['PoolQC'] = df['PoolQC'].fillna('Un')
    df['Fence'] = df['Fence'].fillna('UnKn')
    df['MiscFeature'] = df['MiscFeature'].fillna('Othr')

    df['GarageCars'] = df['GarageCars'].fillna(1)
    df['GarageArea'] = df['GarageArea'].fillna(210)

    df.loc[df['GarageQual'] == 'Ex', 'GarageQual'] = 'Other'
    df.loc[df['PoolQC'] == 'Fa', 'PoolQC'] = 'Ex'

    df.loc[df['MiscFeature'] == 'TenC', 'MiscFeature'] = 'Othr'
    df.loc[df['MiscFeature'] == 'Gar2', 'MiscFeature'] = 'Othr'

    df['SaleType'] = df['SaleType'].fillna('WD')

    return df


@click.command()
@click.argument('path_to_raw', type=click.Path(exists=True))
@click.argument('path_to_interim', type=click.Path())
def preprocess_for_train(path_to_raw: str, path_to_interim: str) -> None:
    """
    Очистка данных для обучения с сохранением.
    Функция принимает основной путь к необработанным файлам
    и путь для сохранения промежуточных очищенных данных.

    Args:
        path_to_raw: путь к папке со скачанными исходными
                     файлами train.csv и test.csv.
        path_to_interim: путь сохранения очищенных данных.

    Returns:

    """

    df = preprocess(pd.read_csv(f'{path_to_raw}'))
    # сохраняем промежуточные данные для обучения
    df.to_csv(f'{path_to_interim}', index=False)

    return


if __name__ == '__main__':
    preprocess_for_train()
