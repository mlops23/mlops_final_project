import click
import pickle
import pandas as pd


@click.command()
@click.argument('path_to_interim', type=click.Path(exists=True))
@click.argument('path_to_processed', type=click.Path())
def prepare_to_train(path_to_interim: str, path_to_processed: str) -> None:
    """
    Подготовка обучающих данных
    из train_interim, сохранение данных в pickle

    Args:
        path_to_interim: путь к папке с предобработанным train_interim.csv
        path_to_processed: путь, по которому сохраняем
                           данные для обучения и теста в pickle.

    Returns:

    """

    train_df = pd.read_csv(f'{path_to_interim}/train_interim.csv')

    # подготовка категориальных признаков для модели lgbm
    categorical = train_df.select_dtypes(['object']).columns
    for i in categorical:
        train_df[i] = train_df[i].astype('category')

    features = train_df.drop('SalePrice', axis=1)
    target = train_df['SalePrice']

    # сохраняем отдельно словарь с типами данных
    dtypes_dict = {col: features[col].dtype for col in features.columns}
    with open(f'{path_to_processed}/dtypes_features.pkl', 'wb') as file:
        pickle.dump(dtypes_dict, file)

    # сохраняем данные в pickle
    for data, file_name in zip((features, target), ('features', 'target')):
        with open(f'{path_to_processed}/{file_name}.pkl', 'wb') as file:
            pickle.dump(data, file)


if __name__ == '__main__':
    prepare_to_train()
